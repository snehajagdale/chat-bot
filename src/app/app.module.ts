import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ROUTING } from './app.routing';

import { AppComponent } from './app.component';
import { ChatComponent } from './components/chat/chat.component';

import { ChatService } from './services/chat.service';
import { AuthnService } from './services/authn.service';
import { InteractService } from './services/interact.service';
import { CommonService } from './services/common.service';
import { DatabaseService } from './services/database.service';
import { DataPersistenceService } from './services/data-persistence.service';
import { StorageService } from './services/storage.service';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { CONFIG } from './config';


@NgModule({
  declarations: [
    AppComponent,
    ChatComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ROUTING,
    AngularFireModule.initializeApp(CONFIG.firebase),
    AngularFireDatabaseModule
  ],
  providers: [ChatService, AuthnService, InteractService, CommonService, DatabaseService, DataPersistenceService, StorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
