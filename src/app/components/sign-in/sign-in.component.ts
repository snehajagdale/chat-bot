import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DataPersistenceService } from '../../services/data-persistence.service';
import { CommonService } from '../../services/common.service';
import { ChatService } from '../../services/chat.service';
import { AuthnService } from '../../services/authn.service';
import { Router } from '@angular/router';
import { InteractService } from '../../services/interact.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  data:any = {};
  loading:boolean = false;
  private password:string = '';
  
  constructor(private dataPersistenceService:DataPersistenceService, private commonService:CommonService, private chatService: ChatService, private authnService: AuthnService, private router: Router, private interactService: InteractService) { }

  ngOnInit() {
    this.data = this.interactService.data;
    console.log(this.data);

  }

  submit() {
    if ((this.data.email).trim() == '' || (this.password).trim() == '') {
      alert('some fields are empty');
      return;
    }else if(!this.commonService.emailValidate(this.data.email)){
      alert('enter valid email');
      return;
    }
    this.signIn(this.data.email, this.password, true);
  }

  signIn(email:string, password:string, allowAlert?:boolean){
    this.loading = true;
    this.authnService.signin({email:email, pass:password}).subscribe((res) => {
      this.loading = false;
      if (res) {
        this.interactService.setData({validUser:res, email:email, username:this.data.username});
        this.dataPersistenceService.storeDataInLocalStorage(this.interactService.data.loginStorageID, {username:this.data.username, email:email, password:password});
        this.interactService.changeData({id:'login', data:true});
      } else {
        this.password = '';
        this.data.email = '';
        this.interactService.setData({email:this.data.email, username:''});
        (allowAlert)?console.error('account already exist, please sign up with new email'):'';
        (allowAlert)?alert('account already exist, please sign up with new email'):'';
      }
    },(err)=>{
      this.loading = false;
      console.error(err, err.code);
      if(err.code === 'auth/user-not-found'){
        console.error(err.message);
        alert(err.message);
      }else if(err.code === 'auth/wrong-password'){
        console.error(err.message);
        alert(err.message);
        this.password = '';
      }else{
        console.error(err);
      }
    });
  }

  navigateTo(page: string) {
    this.commonService.navigateTo(page);
  }

}
